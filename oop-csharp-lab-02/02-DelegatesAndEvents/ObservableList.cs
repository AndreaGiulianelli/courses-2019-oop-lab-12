﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        IList<TItem> list = new List<TItem>();

        public IEnumerator<TItem> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            list.Add(item);
            if (this.ElementInserted != null)
            {
                ElementInserted(this, item, list.IndexOf(item));
            }
        }

        public void Clear()
        {
            foreach(var x in list)
            {
                this.Remove(x);
            }
        }

        public bool Contains(TItem item)
        {
            return list.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            int index = list.IndexOf(item);
            bool res = list.Remove(item);
            if (this.ElementRemoved != null)
            {
                ElementRemoved(this, item, index);
            }
            return res;
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }
        public int IndexOf(TItem item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            list.Insert(index, item);
            if (this.ElementInserted != null)
            {
                ElementInserted(this, item, index);
            }
        }

        public void RemoveAt(int index)
        {
            TItem x = list[index];
            list.RemoveAt(index);
            if (this.ElementRemoved != null)
            {
                ElementRemoved(this, x, index);
            }
        }

        public TItem this[int index]
        {
            get { return list[index]; }
            set
            {
                TItem old = list[index];
                list[index] = value;
                if (this.ElementChanged != null)
                {
                    ElementChanged(this, value, old, index);
                }
            }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}