﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private Dictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach(TKey1 k1 in keys1)
            {
                foreach (TKey2 k2 in keys2)
                {
                    TValue res = generator.Invoke(k1, k2);
                    Tuple<TKey1, TKey2> x = new Tuple<TKey1, TKey2>(k1, k2);
                    values[x] = res;
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return GetElements().Equals(other.GetElements());
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return values[new Tuple<TKey1, TKey2>(key1, key2)];
            }

            set
            {
                values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            List<Tuple<TKey2, TValue>> res = new List<Tuple<TKey2, TValue>>();
            var list = (values
                       .Where((value) => value.Key.Item1.Equals(key1))
                       .Select(value=> new Tuple<TKey2, TValue>(value.Key.Item2, value.Value))).ToList<Tuple<TKey2, TValue>>();

            return res;

        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            List<Tuple<TKey1, TValue>> res = new List<Tuple<TKey1, TValue>>();
            var list = (values
                       .Where((value) => value.Key.Item2.Equals(key2))
                       .Select(value => new Tuple<TKey1, TValue>(value.Key.Item1, value.Value))).ToList<Tuple<TKey1, TValue>>();

            return res;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            List<Tuple<TKey1, TKey2, TValue>> res = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach(var value in values)
            {
                res.Add(new Tuple<TKey1, TKey2, TValue>(value.Key.Item1, value.Key.Item2, value.Value));
            }
            return res;
        }

        public int NumberOfElements
        {
            get
            {
                return values.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
